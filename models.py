# beginning of models.py
# note that at this point you should have created "bookdb" database (see install_postgres.txt).
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgres://postgres:asd123@localhost:5432/bookdb')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True # to suppress a warning message
db = SQLAlchemy(app)

class Book(db.Model):
	__tablename__ = 'book'

	title = db.Column(db.String, nullable = False)
	id = db.Column(db.Integer, primary_key = True)
	publication_date = db.Column(db.String)
	author = db.Column(db.String)
	publisher = db.Column(db.String)
	cover = db.Column(db.String)

class Author(db.Model):
	__tablename__ = 'author'

	a_name = db.Column(db.String, nullable = True)
	a_id =  db.Column(db.Integer, primary_key = True)
	date_of_birth = db.Column(db.String, nullable = True)
	nationality = db.Column(db.String, nullable = True)
	a_website = db.Column(db.String, nullable = True)
	a_image = db.Column(db.String, nullable = True)

class Publisher(db.Model):
	__tablename__ = 'publisher'

	p_name = db.Column(db.String, nullable = True)
	p_id = db.Column(db.Integer, primary_key = True)
	founded = db.Column(db.String, nullable = True)
	p_website = db.Column(db.String, nullable = True)
	description = db.Column(db.String, nullable = True)
	p_image = db.Column(db.String, nullable = True)

db.drop_all()
db.create_all()
# End of models.py
