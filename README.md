# cs329e-idb

CS329e, Group 6
This repo creates a dynamic website that displays information about books, authors and
publishers in a responsive way.

The json folder contains books.html and books.json, which are used for the database to dynamically display 
the information in the database.

The static folder contains the css folder and the images folder. The css folder contains a css style sheet.
The images folder conatins all the images we use on our website.

The templates folder contains the hmtl templates for all of our pages, and a few extra pages that are not
implemented. This includes our templates for the pages we would have implemented if we got linking to individual
data points to work.

The main.py file contains the flask import and sets up the website structure, as well as the backbone for interacting
with the database.

Models.py contains our models for the database. This is how objects will be displayed in the database. Create_db.py sets up the database
and fills it.

tests.py contains unit tests. These run on gitlab CI by using .gitlab-ci.yaml.

You can run the application by cloning the directory, making sure you are in the main directory
and running the command "python3 main.py" or "python main.py". Locally this will provide you with a
link to the development server.

We will also deploy the website on GCP. The app.yaml is used to set up the GCP instance.