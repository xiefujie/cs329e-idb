# beginning of models.py
# note that at this point you should have created "bookdb" database (see install_postgres.txt).
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from models import Book, Author, Publisher
from create_db import db
import unittest


class TestBook (unittest.TestCase) :
    def test_make_book_1 (self):
            book1 = Book(title = "Bralen's BOOK", id = 0, publication_date = "2019", author = "Bralen", publisher = "Bralen's MOM", cover = "https://upload.wikimedia.org/wikipedia/en/a/a4/Flag_of_the_United_States.svg")
            db.session.add(book1)
            db.session.commit()

    def test_added_book_1 (self):
            x = db.session.query(Book).filter_by(id = "0").one()
            self.assertEqual(x.id, 0)

    def test_delete_book_1 (self):
            db.session.query(Book).filter_by(id = '0').delete()
            db.session.commit()


class TestAuthor (unittest.TestCase) :
    def test_make_author_1 (self):
            author1 = Author(a_name = "Bralen's BOOK", a_id = 0, date_of_birth = "2019", nationality = "Bralen", a_website = "Bralen's MOM", a_image = "https://upload.wikimedia.org/wikipedia/en/a/a4/Flag_of_the_United_States.svg")
            db.session.add(author1)
            db.session.commit()

    def test_added_author_1 (self):
            x = db.session.query(Author).filter_by(a_id = "0").one()
            self.assertEqual(x.a_id, 0)

    def test_delete_author_1 (self):
            db.session.query(Author).filter_by(a_id = '0').delete()
            db.session.commit()


class TestPublisher (unittest.TestCase) :
    def test_make_publisher_1 (self):
            publisher1 = Publisher(p_name = "Bralen's BOOK", p_id = 0, founded = "2019", p_website = "google.com", description = "Bralen's MOM", p_image = "https://upload.wikimedia.org/wikipedia/en/a/a4/Flag_of_the_United_States.svg")
            db.session.add(publisher1)
            db.session.commit()

    def test_added_publisher_1 (self):
            x = db.session.query(Publisher).filter_by(p_id = "0").one()
            self.assertEqual(x.p_id, 0)

    def test_delete_publisher_1 (self):
            db.session.query(Publisher).filter_by(p_id = '0').delete()
            db.session.commit()

if __name__ == "__main__" :

    unittest.main()
