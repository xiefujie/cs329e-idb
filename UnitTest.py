import os
import sys
import unittest
#from models import db, Book
from create_db import db, Book

class DBTestCases(unittest.TestCase):
    def test_source_insert_1(self):
        # insert Book into database
        # need to edit test to include author, publisher, etc.
        s = Book(id='20', title = 'C++')
        db.session.add(s)
        db.session.commit()

        # Check that book is added into database properly via ID #
        r = db.session.query(Book).filter_by(id = '20').one()
        self.assertEqual(str(r.id), '20')
        
        # Remove book from database
        db.session.query(Book).filter_by(id = '20').delete()
        db.session.commit()


        # other possible tests:
        # Tests made by Chase:
        # Tests to ensure endpoints are live
        # make sure the app's api call returns the same data as an independent read from our db
        # ensure that authors,publishers, books are linked correctly
        # Adding authors/ publishers
        # 
        # adding a book that is already within the database
        # adding authors/publishers that are withing the database
        # 
if __name__ == '__main__':
    unittest.main()
